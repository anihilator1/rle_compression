#include <iostream>
#include <numeric>
#include <stdlib.h>
#include <list>
#include<time.h>
#include<fstream>
#include<iomanip>
#include<string.h>
#include<vector>
#include<chrono>
#include<unistd.h>
#include<typeinfo>
static void CheckCudaErrorAux (const char *, unsigned, const char *, cudaError_t);
#define CUDA_CHECK_RETURN(value) CheckCudaErrorAux(__FILE__,__LINE__, #value, value)


using namespace std;


static void CheckCudaErrorAux (const char *file, unsigned line, const char *statement, cudaError_t err)
{
	if (err == cudaSuccess)
		return;
	std::cerr << statement<<" returned " << cudaGetErrorString(err) << "("<<err<< ") at "<<file<<":"<<line << std::endl;
	exit (1);
}

__host__ __device__ void printTab(int* tab, int size)
{
	for(int i=0;i<size;i++)
	{
		printf("%d ",tab[i]);
	}
	printf("\n");
}



void testExample(int* tab)
{
	tab[0]=1;
	tab[1]=2;
	tab[2]=3;
	tab[3]=6;
	tab[4]=6;
	tab[5]=6;
	tab[6]=5;
	tab[7]=5;
}

__device__ void clearMemory(int idb,int blockDim, int *data)
{
	data[idb]=0;
	__syncthreads();
}

__device__ void globalToSharedMemory(unsigned idx,unsigned idb,int *gpuDataGlobal,int *gpuDataShared)
{
	gpuDataShared[idb]=gpuDataGlobal[idx];
	__syncthreads();
}

__device__ void computeBackwardMask(int idb,int *gpuDataShared,int *backwardMask)
{
	if(idb==0)
	{
		backwardMask[idb]=1;
	}
	else
	{
		if(gpuDataShared[idb-1]!=gpuDataShared[idb])
		{
			backwardMask[idb]=1;
		}
	}
}
__device__ void prescan(int thid,int *temp, unsigned n)
{
	int offset = 1;
	for (int d = n>>1; d > 0; d >>= 1)
	{
	__syncthreads();
	   if (thid < d)
	   {
		   int ai = offset*(2*thid+1)-1;
		   int bi = offset*(2*thid+2)-1;
		   temp[bi] += temp[ai];
	   }
	   offset *= 2;
	}
	if (thid == 0) { temp[n - 1] = 0; }
	for (int d = 1; d < n; d *= 2)
	{
	     offset >>= 1;
	     __syncthreads();
	     if (thid < d)
	     {
	    	 int ai = offset*(2*thid+1)-1;
	    	 int bi = offset*(2*thid+2)-1;
	    	 float t = temp[ai];
	    	 temp[ai] = temp[bi];
	    	 temp[bi] += t;
	     }
	}
	__syncthreads();
}
__device__ void scan(int thid,int *backwardMask,int *scannedBackwardMask, unsigned n)
{
	scannedBackwardMask[thid]=backwardMask[thid];
	__syncthreads();
	prescan(thid,scannedBackwardMask,n);
	scannedBackwardMask[thid]+=backwardMask[thid];
	__syncthreads();
}
__device__ void computeCompactedBackwardMask(int idx,int idb,int *backwardMask, int *scannedBackwardMask,int *compactedBackwardMask,int *elementNumber,unsigned size)
{
		if(idb==0)
		{
			compactedBackwardMask[0]=0;
		}
		else
		{
			if(backwardMask[idb]==1)
				compactedBackwardMask[scannedBackwardMask[idb]-1]=idb;
		}
		if(idb==size-1)
		{
			compactedBackwardMask[scannedBackwardMask[idb]]=size;
			*elementNumber=scannedBackwardMask[idb];
		}
		__syncthreads();
}
__device__ void computeResult(int idb,int *gpuData,int *compactedBackwardMask,int *gpuResultSymbol,int *gpuResultNumber, int* elementNumber)
{
	gpuResultNumber[idb]=0;
	if(*elementNumber>idb)
	{
		gpuResultSymbol[idb]=gpuData[compactedBackwardMask[idb]];
		gpuResultNumber[idb]=compactedBackwardMask[idb+1]-compactedBackwardMask[idb];
	}
	__syncthreads();
}
__device__ void sharedToGlobalMemory(unsigned idx,unsigned idb,int *gpuResultSymbol,int *gpuResultNumber,int *gpuResultSymbolGlobal,int *gpuResultNumberGlobal)
{
	gpuResultSymbolGlobal[idx]=gpuResultSymbol[idb];
	gpuResultNumberGlobal[idx]=gpuResultNumber[idb];
	__syncthreads();
}
__global__ void mainKernel(int *gpuDataGlobal,int *gpuResultSymbolGlobal,int *gpuResultNumberGlobal,unsigned size)
{
	unsigned idx = blockIdx.x*blockDim.x+threadIdx.x;
	unsigned idb = threadIdx.x;
	__shared__ int sharedMemory[4*256];
	int *elementNumber=sharedMemory+4*blockDim.x+1;
	int *gpuDataShared=sharedMemory;
	int *backwardMask=sharedMemory+blockDim.x;
	int *scannedBackwardMask=sharedMemory+2*blockDim.x;
	int *compactedBackwardMask=sharedMemory+3*blockDim.x;
	int *gpuResultSymbol;
	int *gpuResultNumber;
	clearMemory(idb,blockDim.x,backwardMask);
	if(idx<size)
	{
		globalToSharedMemory(idx,idb,gpuDataGlobal,gpuDataShared);
		computeBackwardMask(idb,gpuDataShared,backwardMask);

	}
		scan(idb,backwardMask,scannedBackwardMask,blockDim.x);
	if(idx<size)
	{
		computeCompactedBackwardMask(idx,idb,backwardMask,scannedBackwardMask,compactedBackwardMask,elementNumber,size-(blockIdx.x*blockDim.x)>blockDim.x?blockDim.x:size-(blockIdx.x*blockDim.x));
		gpuResultSymbol=backwardMask;
		gpuResultNumber=scannedBackwardMask;
		computeResult(idb,gpuDataShared,compactedBackwardMask,gpuResultSymbol,gpuResultNumber,elementNumber);
		sharedToGlobalMemory(idx,idb,backwardMask,scannedBackwardMask,gpuResultSymbolGlobal,gpuResultNumberGlobal);

	}
}

typedef struct
{
	int symbol;
	int count;
} result_pair;

list<result_pair> converter(int *symbol,int * number,unsigned size)
{
	list<result_pair> list;
	for(int i=0;i<size;i++)
	{
		if(number[i]!=0)
		{
			result_pair tmp;
			tmp.symbol=symbol[i];
			tmp.count=number[i];
			list.push_back(tmp);
		}
	}
	return list;
}
list<result_pair> initKernel(int *cpuData, unsigned size,int *time, int BLOCK_SIZE)
{

	int *cpuResultSymbol = new int[size];
	int *cpuResultNumber = new int[size];
	int *gpuData;
	int *gpuResultSymbol;
	int *gpuResultNumber;

	CUDA_CHECK_RETURN(cudaMalloc((void **)&gpuData, sizeof(int)*size));
	CUDA_CHECK_RETURN(cudaMalloc((void **)&gpuResultSymbol, sizeof(int)*size));
	CUDA_CHECK_RETURN(cudaMalloc((void **)&gpuResultNumber, sizeof(int)*size));
	CUDA_CHECK_RETURN(cudaMemcpy(gpuData, cpuData, sizeof(int)*size, cudaMemcpyHostToDevice));

	const int blockCount = (size+BLOCK_SIZE-1)/BLOCK_SIZE;

	auto started = std::chrono::high_resolution_clock::now();

	mainKernel<<<blockCount, BLOCK_SIZE,4*BLOCK_SIZE*sizeof(int)+2*sizeof(int)>>> (gpuData,gpuResultSymbol,gpuResultNumber, size);

	auto done = std::chrono::high_resolution_clock::now();

	CUDA_CHECK_RETURN(cudaMemcpy(cpuResultSymbol, gpuResultSymbol, sizeof(int)*size, cudaMemcpyDeviceToHost));
	CUDA_CHECK_RETURN(cudaMemcpy(cpuResultNumber, gpuResultNumber, sizeof(int)*size, cudaMemcpyDeviceToHost));
	CUDA_CHECK_RETURN(cudaFree(gpuData));
	CUDA_CHECK_RETURN(cudaFree(gpuResultSymbol));
	CUDA_CHECK_RETURN(cudaFree(gpuResultNumber));

	*time=std::chrono::duration_cast<std::chrono::nanoseconds>(done-started).count();
	return converter(cpuResultSymbol,cpuResultNumber,size);
}
void printList(list<result_pair> list)
{
	for(auto it=list.begin();it!=list.end();++it)
	{
		cout<<"("<<(*it).symbol<<" "<<(*it).count<<")"<<" ";
	}
	cout<<"\n";
}
//main(int argc,char** argv)
//{
//	srand(time(NULL));
//	int size=65;//atoi(argv[1]);
//	int* data=new int[size];
//	//testExample(data);
//	generateExample(data,size);
//	list<result_pair> cpuResult=initKernel(data,size);
//	printList(cpuResult);
//	delete[] data;
//}
void generateExample(int *data, int size,int compresabiltiy)
{
	if(size==0) return;
	int next=rand()%1000;
	data[0]=next;
	for(int i=1;i<size;i++)
	{
		if(compresabiltiy<rand()%100)
		{
			next=rand()%1000;
		}
		data[i]=next;
	}
	return;
}
void exampleFromFile(int **data,int *size,char * fileName)
{
	std::ifstream file;
	file.open(fileName);
	int a;
	std::vector<int> v;
	while(file>>a)
	{
		v.push_back(a);
	}
	*size=v.size();
	*data=new int[v.size()];
	for(int i=0;i<v.size();i++)
	{
		(*data)	[i]=v[i];
	}
	file.close();
}
int main(int argc,char** argv)
{
	char c;
	int generatorOrFile=1;
	int size=10;
	int compressability=10;
	int compressionRate=0;
	int output=0;
	int time=0;
	int BLOCK_SIZE=128;
	char* fileName;
	srand(clock());
	while ((c = getopt (argc, argv, "S:ots:c:f:rb:")) != -1)
		switch (c)
		{
			case 'S':
				if(strcmp(optarg,"gen")==0) generatorOrFile=1;
				if(strcmp(optarg,"file")==0) generatorOrFile=2;
				//else return EXIT_FAILURE;
				break;
			case 'o':
				output=1;
				break;
			case 't':
				time=1;
				break;
			case 's':
				size=atoi(optarg);
				break;
			case 'c':
				compressability=atoi(optarg);
				break;
			case 'f':
				fileName=optarg;
				break;
			case 'r':
				compressionRate=1;
				break;
			case 'b':
				BLOCK_SIZE=atoi(optarg);
				break;
		}

	int* data;
	int execTime;
	if(generatorOrFile==1)
	{
		data=new int[size];
		generateExample(data,size,compressability);
	}
	else
	{
		exampleFromFile(&data,&size,fileName);
	}
	list<result_pair> result=initKernel(data,size,&execTime,BLOCK_SIZE);
	if(output==1) printList(result);

	if(time==1) std::cout <<"executed in:"<<execTime<<" nanoseconds\n";
	if(compressionRate==1) std::cout <<"Compressed data to orginal:"<< std::setprecision (2) << std::fixed << ((double)result.size()*2)/size<<"\n";
	std::cout<<"\n";
	delete[] data;
	return EXIT_SUCCESS;
}


